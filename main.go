package main

import (
	"bake/controller"
	"bake/model"
	"bufio"
	"fmt"
	"os"
	"sort"
)

var productList = []model.Product{
	model.Product{
		Name:  "Vegemite Scroll",
		Code:  "VS5",
		Packs: 3,
		Price: 6.99,
	},
	model.Product{
		Name:  "Vegemite Scroll",
		Code:  "VS5",
		Packs: 5,
		Price: 8.99,
	},
	model.Product{
		Name:  "Blueberry Muffin",
		Code:  "MB11",
		Packs: 2,
		Price: 9.95,
	},
	model.Product{
		Name:  "Blueberry Muffin",
		Code:  "MB11",
		Packs: 5,
		Price: 16.95,
	},
	model.Product{
		Name:  "Blueberry Muffin",
		Code:  "MB11",
		Packs: 8,
		Price: 24.95,
	},
	model.Product{
		Name:  "Croissant",
		Code:  "CF",
		Packs: 3,
		Price: 5.95,
	},
	model.Product{
		Name:  "Croissant",
		Code:  "CF",
		Packs: 5,
		Price: 9.95,
	},
	model.Product{
		Name:  "Croissant",
		Code:  "CF",
		Packs: 9,
		Price: 16.99,
	},
}

func main() {

	for true {
		// Read console input
		reader := bufio.NewReader(os.Stdin)
		fmt.Println("+----------------------------------------------+")
		fmt.Print("Product Qty Product Code: ")

		text, err := reader.ReadString('\n')

		if err != nil {
			fmt.Printf("%s", err.Error())
			return
		}
		if len(text) == 1 {
			fmt.Println("Please type [Product Qty] [Product Code] ")
			return
		}

		// Parse console input
		desired, productCode, err := controller.InputHandler(text)
		if err == nil {
			// Filter product based on current code
			product, pack, err := controller.FilterProductByCode(productList, productCode)

			if err == nil {
				// factor array based on desired number with current filtered
				// product pack array
				f, _ := controller.Factors(desired, pack)
				sort.Sort(sort.Reverse(sort.IntSlice(f)))

				// Present to user a visual result and merge both arrays.
				var total float32
				fmt.Println("+----------------------------------------------+")
				fmt.Println("+    Unit    |    Pack    |    Price Unit      +")
				fmt.Println("+----------------------------------------------+")
				for _, p := range product {
					qty, err := controller.Ocurrences(f, p.Packs)
					if err == nil {
						if qty != 0 {
							total += (float32(qty) * p.Price)
							fmt.Printf("+     %d      |     %d      |       $%.2f       +\n", qty, p.Packs, p.Price)
							fmt.Print("+----------------------------------------------+\n")
						}
					}
				}
				fmt.Println("+     TOTAL    +")
				fmt.Println("+--------------+")
				fmt.Printf("+     $%.2f    +\n", total)
				fmt.Println("+--------------+")
			}
		}
		text = ""
	}
}
