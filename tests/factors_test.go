package tests

import (
	"bake/controller"
	"bake/store"
	"testing"

	"github.com/google/go-cmp/cmp"
)

// TestFactors test this function to prevent errors
func TestFactorsParameters(t *testing.T) {

	var tests = []struct {
		number   int
		packs    []int
		expected error
	}{
		{0, []int{3, 5}, store.ErrZeroNumber},
		{-10, []int{3, 5}, store.ErrNegativeNumber},
		{10, []int{}, store.ErrEmptyArray},
		{10, []int{3, -5}, store.ErrNegativeNumArray},
		{10, []int{3, 0}, store.ErrArrayZero},
	}
	for _, test := range tests {
		if _, err := controller.Factors(test.number, test.packs); err != test.expected {
			t.Fail()
			t.Logf("Expected '%s', got '%s'", test.expected, err.Error())
		}
	}
}

// Test if Factor function is able to return expected integer number array.
func TestFactorsSum(t *testing.T) {
	var tests = []struct {
		number   int
		packs    []int
		expected []int
	}{
		{10, []int{3, 5}, []int{5, 5}},
		{14, []int{2, 5, 8}, []int{2, 2, 2, 8}},
		{13, []int{3, 5, 9}, []int{3, 5, 5}},
	}
	for _, test := range tests {
		if f, _ := controller.Factors(test.number, test.packs); cmp.Equal(f, test.expected) == false {
			t.Fail()
			t.Logf("Expected '%v', got '%v'", test.expected, f)
		}
	}
}
