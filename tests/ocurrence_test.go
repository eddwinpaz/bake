package tests

import (
	"bake/controller"
	"bake/store"
	"testing"
)

// Test how many numbers are inside a integer array and get the total count
func TestOcurrenceParameters(t *testing.T) {

	var tests = []struct {
		array    []int
		number   int
		expected error
	}{
		{[]int{5, 5, 5, 5, -5}, 0, store.ErrZeroNumber},
		{[]int{5, 5, 5, 5, -5}, -1, store.ErrNegativeNumber},
		{[]int{}, 5, store.ErrEmptyArray},
		{[]int{5, 5, 5, 5, 0}, 5, store.ErrArrayZero},
		{[]int{5, 5, 5, 5, -5}, 5, store.ErrNegativeNumArray},
	}

	for _, test := range tests {
		if _, err := controller.Ocurrences(test.array, test.number); err != test.expected {
			t.Fail()
			t.Logf("Expected '%s', got '%s'", test.expected, err)
		}
	}
}
