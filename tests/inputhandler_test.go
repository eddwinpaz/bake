package tests

import (
	"bake/controller"
	"bake/store"
	"testing"
)

// Test Input Handler and make sure if follows validation steps.
func TestInputHandlerParameters(t *testing.T) {

	var tests = []struct {
		input    string
		expected error
	}{
		{"", store.ErrEmptyValue},
		{"10", store.ErrNoMatchLine},
		{"R VS5", store.ErrNotNumber},
	}

	for _, test := range tests {
		if _, _, err := controller.InputHandler(test.input); err != test.expected {
			t.Fail()
			t.Logf("Expected '%s', got '%s'", test.expected, err)
		}
	}
}

func TestInputHandlerReturns(t *testing.T) {

	var tests = []struct {
		input    string
		expected int
	}{
		{"", 0},
		{"10", 0},
		{"R VS5", 0},
	}

	for _, test := range tests {

		num, code, _ := controller.InputHandler(test.input)

		if num != 0 {
			t.Fail()
			t.Logf("Expected '%d', got '%d'", test.expected, 0)
		} else if len(code) != 0 {
			t.Fail()
			t.Logf("Expected '%d', got '%d'", test.expected, len(code))
		}
	}
}
