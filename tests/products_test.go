package tests

import (
	"bake/controller"
	"bake/model"
	"bake/store"
	"testing"
)

var productList = []model.Product{
	model.Product{
		Name:  "Vegemite Scroll",
		Code:  "VS5",
		Packs: 3,
		Price: 6.99,
	},
	model.Product{
		Name:  "Vegemite Scroll",
		Code:  "VS5",
		Packs: 5,
		Price: 8.99,
	},
	model.Product{
		Name:  "Blueberry Muffin",
		Code:  "MB11",
		Packs: 2,
		Price: 9.95,
	},
	model.Product{
		Name:  "Blueberry Muffin",
		Code:  "MB11",
		Packs: 5,
		Price: 16.95,
	},
	model.Product{
		Name:  "Blueberry Muffin",
		Code:  "MB11",
		Packs: 8,
		Price: 24.95,
	},
	model.Product{
		Name:  "Croissant",
		Code:  "CF",
		Packs: 3,
		Price: 5.95,
	},
	model.Product{
		Name:  "Croissant",
		Code:  "CF",
		Packs: 5,
		Price: 9.95,
	},
	model.Product{
		Name:  "Croissant",
		Code:  "CF",
		Packs: 9,
		Price: 16.99,
	},
}

// TestFactors test this function to prevent errors when passing parameters
func TestFilterProdByCodeParameter(t *testing.T) {

	var tests = []struct {
		list     []model.Product
		code     string
		expected error
	}{
		{[]model.Product{}, "VS5", store.ErrEmptyArray},
		{productList, "", store.ErrEmptyValue},
	}

	for _, test := range tests {
		if _, _, err := controller.FilterProductByCode(test.list, test.code); err != test.expected {
			t.Fail()
			t.Logf("(Expected '%s', got '%s'", test.expected, err)
		}
	}

}

// Test filter product by code and validate it's returning value
func TestFilterProdByCodeReturnObject(t *testing.T) {

	var expectedArray = []model.Product{
		model.Product{
			Name:  "Vegemite Scroll",
			Code:  "VS5",
			Packs: 3,
			Price: 6.99,
		},
		model.Product{
			Name:  "Vegemite Scroll",
			Code:  "VS5",
			Packs: 5,
			Price: 8.99,
		},
	}

	var expectedNumArray = []int{3, 5}

	pack, num, err := controller.FilterProductByCode(productList, "VS5")
	if pack == nil {
		t.Fail()
		t.Logf("Expected '%v' got '%v", expectedArray, pack)
	} else if num == nil {
		t.Fail()
		t.Logf("Expected '%v' got '%v", expectedNumArray, num)
	} else if err != nil {
		t.Fail()
		t.Logf("Expected '%s' got '%s", "", err.Error())
	}
}
