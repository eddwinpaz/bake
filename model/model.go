package model

// Product contains basic information structure
// that needs to be stored temporaly.
type Product struct {
	Name  string  // Product Name
	Code  string  // Product Code
	Packs int     // Quantity of bread sitting for this product
	Price float32 // Price set
}

// Error display custom error messages
type Error interface {
	Error() string
}
