package controller

import (
	"bake/model"
	"bake/store"
	"sort"
	"strconv"
	"strings"
)

// InputHandler handle input from console.
func InputHandler(inputValue string) (int, string, error) {

	// check parameters conditions
	if len(inputValue) == 0 {
		return 0, "", store.ErrEmptyValue

	} else if temp := strings.Split(inputValue, " "); len(temp) != 2 {
		return 0, "", store.ErrNoMatchLine
	}
	// check structure value
	value := strings.Split(inputValue, " ")
	number, err := strconv.Atoi(value[0])

	if err != nil {
		return 0, "", store.ErrNotNumber
	} else if len(value[1]) == 0 {
		return 0, "", store.ErrEmptyValue
	}

	return number, strings.TrimSpace(value[1]), nil
}

//FilterProductByCode filter product by code identifier
func FilterProductByCode(list []model.Product, code string) ([]model.Product, []int, error) {

	if len(list) == 0 {
		return nil, nil, store.ErrEmptyArray
	} else if len(code) == 0 {
		return nil, nil, store.ErrEmptyValue
	}
	var packObject = []model.Product{}
	var packNumbers = []int{}

	for _, prod := range list {
		if prod.Code == code {
			packObject = append(packObject, model.Product{
				Name:  prod.Name,
				Code:  prod.Code,
				Packs: prod.Packs,
				Price: prod.Price,
			})
			packNumbers = append(packNumbers, prod.Packs)
		}
	}
	return packObject, packNumbers, nil
}

// Factors takes a count of pack numeration and returns a integer array if successful
func Factors(desired int, numbers []int) ([]int, error) {
	// parameter validation
	if desired == 0 {
		return []int{}, store.ErrZeroNumber
	} else if desired <= -1 {
		return nil, store.ErrNegativeNumber
	} else if len(numbers) == 0 {
		return nil, store.ErrEmptyArray
	}

	for _, num := range numbers {
		if num == 0 {
			return nil, store.ErrArrayZero
		} else if num <= -1 {
			return nil, store.ErrNegativeNumArray
		}
	}

	// pack numeration recursively
	sort.Sort(sort.Reverse(sort.IntSlice(numbers)))
	for _, number := range numbers {
		f, _ := Factors(desired-number, numbers)
		if f != nil {
			f = append(f, number)
			return f, nil
		}
	}
	return nil, nil
}

// Ocurrences find how many elements same number are inside array
func Ocurrences(arr []int, num int) (int, error) {

	// parameter validation
	if num == 0 {
		return 0, store.ErrZeroNumber
	} else if num <= -1 {
		return 0, store.ErrNegativeNumber
	} else if len(arr) == 0 {
		return 0, store.ErrEmptyArray
	}

	for _, num := range arr {
		if num == 0 {
			return 0, store.ErrArrayZero
		} else if num <= -1 {
			return 0, store.ErrNegativeNumArray
		}
	}

	res := 0
	for _, val := range arr {
		if num == val {
			res++
		}
	}
	return res, nil
}
