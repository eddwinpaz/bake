# Bake Application (test)
This application job is to return packs of bread based on a request number

# How to use it?
Install it by using following command:

`$ go install gitlab.com/eddwinpaz/bake.git`

`$ go build `

`$ go run bake`

# how to exit code ?

> type control + c  

# Test results 1 - VS5 10

| unit  | pack | price unit |
| ------ | ------ | ------ |
| 2     | 5 | $8.99 |
| TOTAL |  
| $17.98 | 


# Test results 2  - 14 MB11

| unit  | pack | price unit |
| ------ | ------ | ------ |
| 3     | 2 | $9.95 |
| 1     | 8 | $24.95 |
| TOTAL |  
| $54.80 | 



# Test results 3 -  13 CF

| unit  | pack | price unit |
| ------ | ------ | ------ |
| 1     | 3 | $5.95 |
| 2     | 5 | $9.95 |
| TOTAL |  
| $25.85 | 
