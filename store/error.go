package store

import "errors"

// Error codes returned by failures to parse an expression.
// ErrZeroParameter appears when user intens to pass a 0 value
var (
	ErrZeroNumber       = errors.New("value cannot be zero")
	ErrNotNumber        = errors.New("value is not a number")
	ErrNegativeNumber   = errors.New("value cannot be negative")
	ErrEmptyValue       = errors.New("value cannot be empty")
	ErrEmptyArray       = errors.New("array cannot be empty")
	ErrArrayZero        = errors.New("array cannot contain zero")
	ErrNegativeNumArray = errors.New("array cannot contain negative numbers")
	ErrNoMatchLine      = errors.New("line does not contain 2 parameters")
)
